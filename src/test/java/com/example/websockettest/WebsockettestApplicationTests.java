package com.example.websockettest;

import com.example.websockettest.beans.MessageType;
import com.example.websockettest.beans.ParameterBean;
import com.example.websockettest.databaseapi.StatusMessageRepository;
import com.example.websockettest.utils.MyUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class WebsockettestApplicationTests {

    @Autowired
    private static StatusMessageRepository statusMessageRepository;
    @Autowired
    private MockMvc mockMvc ;

    //问题：启动报错
    @Test
    void contextLoads() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get ("/sendMessage/ykq-000001?info=dad").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect((ResultMatcher) content().string(equalTo("设备不在线!")));
    }

    public static void main(String[] arg){
//        testAutoWrite();
    }

    private static void testJson(){
        MessageType messageType = new MessageType(1);
//        messageType.setMessageType(0);
        ParameterBean parameterBean = new ParameterBean();
        parameterBean.setIccid("1235");
        parameterBean.setFishPondName("xxx");

        messageType.setParameterBean(parameterBean);


        String messageTypeJson = MyUtils.obj2stringForGson(messageType);
        O.o("messageTypeJson为：" + messageTypeJson);

        if(MyUtils.validate(messageTypeJson)){
            MessageType messageType1 = MyUtils.string2Obj(messageTypeJson, MessageType.class);
            O.o("messageType1:" + messageType1.toString());
        }
    }

    private static void testAutoWrite(){
        if(statusMessageRepository != null){
            O.o("statusMessageRepository不为空!");
        }else{
            O.o("statusMessageRepository为空啊!");
        }
    }

}
