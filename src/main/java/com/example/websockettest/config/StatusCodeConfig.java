package com.example.websockettest.config;

public class StatusCodeConfig {

    /**
     * 1023
     */

    /**
     * 登录相关
     *
     */
    //登录成功
    public static final int LOGIN_SUCCESS = 1000;
    //无该用户
    public static final int LOGON_NO_USER = 1001;
    //用户名与密码不匹配
    public static final int LOGON_USERNAME_PASSWORD_ERROR = 1002;

    /**
     * 注册相关
     *
     */
    //注册成功
    public static final int REGISTER_SUCCESS = 1003;
    //bean为空、数据缺失、特殊原因
    public static final int REGISTER_FAIL = 1004;
    //手机格式格式错误
    public static final int REGISTER_FAIL_PHONE_FORMAT_ERROR = 1005;
    //该手机号已注册
    public static final int REGISTER_FAIL_PHONE_NUM_ED = 1006;
    //iccid错误
    public static final int REGISTER_FAIL_ICCID_ERROR = 1009;
    //iccid已绑定
    public static final int REGISTER_FAIL_ICCID_BIND = 1010;

    /**
     * 注销相关
     *
     */
    //用户注销成功
    public static final int DELETE_ACCOUNT_SUCCESS = 1007;
    //用户注销失败，无权限、无用户
    public static final int DELETE_ACCOUNT_FAIL = 1008;

    /**
     * 获取设备参数相关
     *
     */
    public static final int PARAM_WORD_LOST = 1011;
    public static final int PARAM_WORD_ERROR = 1012;
    public static final int PARAM_ICCID_NOT_FOUND = 1014;
    public static final int PARAM_ICCID_PHONE_NOT_MATCH = 1015;
    public static final int PARAM_ICCID_DEVICE_NULL = 1016;
    public static final int PARAM_SESSION_KEY_NOT_MATCH = 1017;
    //获取设备参数成功！
    public static final int PARAM_GET_SUCCESS = 1013;

    /**
     * 绑定相关
     *
     */
    //你已经绑定
    public static final int BIND_YOU_HAD_BIND = 1018;
    //绑定成功
    public static final int BIND_SUCCESS = 1019;
    //该iccid已经被人绑定
    public static final int BIND_ICCID_HAD_BIND = 1020;
    //字段缺失
    public static final int BIND_PARAM_LOST = 1021;
    //字段错误
    public static final int BIND_PARAM_ERROR = 1022;
    //sessionKey和iccid不匹配
    public static final int BIND_SESSION_KEY_NOT_MATCH = 1023;

    /**
     * 获取设备状态
     *
     */
    public static final int GET_DEVICE_STATUS_SUCCESS = 1024;
    //
    public static final int GET_DEVICE_STATUS_PARAM_LOST = 1025;
    //
    public static final int GET_DEVICE_STATUS_PARAM_ERROR = 1026;
    //
    public static final int GET_DEVICE_STATUS_SESSION_KET_NOT_MATCH = 1027;
    //
    public static final int GET_DEVICE_STATUS_ICCID_NOT_FOUND = 1028;
    //
    public static final int GET_DEVICE_STATUS_ICCID_USER_NOT_MATCH = 1029;
    //
    public static final int GET_DEVICE_STATUS_DEVICE_NOT_ONLINE = 1030;

    /**
     * 解绑设备相关
     *
     */
    //解绑成功！
    public static final int UNBIND_DEVICE_SUCCESS = 1031;
    //解绑失败
    public static final int UNBIND_DEVICE_FAIL = 1032;

}
