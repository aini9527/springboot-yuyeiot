package com.example.websockettest.beans;

import javax.persistence.*;

@Entity
@Table(name = "ALarmBean01")
public class ALarmBean01 {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String iccid;

    private Long dateForLong;

    private int alarmType;

    private int alarmFlag;

    private int controlBtnIndex;

    private int switchFlag;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIccid() {
        return iccid;
    }

    public void setIccid(String iccid) {
        this.iccid = iccid;
    }

    public Long getDateForLong() {
        return dateForLong;
    }

    public void setDateForLong(Long dateForLong) {
        this.dateForLong = dateForLong;
    }

    public int getAlarmType() {
        return alarmType;
    }

    public void setAlarmType(int alarmType) {
        this.alarmType = alarmType;
    }

    public int getControlBtnIndex() {
        return controlBtnIndex;
    }

    public void setControlBtnIndex(int controlBtnIndex) {
        this.controlBtnIndex = controlBtnIndex;
    }

    public int getSwitchFlag() {
        return switchFlag;
    }

    public void setSwitchFlag(int switchFlag) {
        this.switchFlag = switchFlag;
    }

    public int getAlarmFlag() {
        return alarmFlag;
    }

    public void setAlarmFlag(int alarmFlag) {
        this.alarmFlag = alarmFlag;
    }
}
