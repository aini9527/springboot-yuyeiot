package com.example.websockettest.beans;


import javax.persistence.*;

@Entity
@Table(name = "ParameterBean")
public class ParameterBean {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String iccid;

    private String controlBtnNames;

    private String fishPondName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIccid() {
        return iccid;
    }

    public void setIccid(String iccid) {
        this.iccid = iccid;
    }

    public String getControlBtnNames() {
        return controlBtnNames;
    }

    public void setControlBtnNames(String controlBtnNames) {
        this.controlBtnNames = controlBtnNames;
    }

    public String getFishPondName() {
        return fishPondName;
    }

    public void setFishPondName(String fishPondName) {
        this.fishPondName = fishPondName;
    }

    @Override
    public String toString() {
        return "ParameterBean{" +
                "id=" + id +
                ", iccid='" + iccid + '\'' +
                ", controlBtnNames='" + controlBtnNames + '\'' +
                ", fishPondName='" + fishPondName + '\'' +
                '}';
    }
}
