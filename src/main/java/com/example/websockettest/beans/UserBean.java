package com.example.websockettest.beans;

import javax.persistence.*;

@Entity
@Table(name = "UserBean")
public class UserBean {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = true)
    private String userName;
    @Column(nullable = true)
    private String userDeviceIccid;
    @Column(nullable = true, unique = true)
    private Long userPhonenum;
    @Column(nullable = true)
    private String password;
    @Column(nullable = true)
    private String createTime;
    @Column(nullable = true)
    private String userFishPondName;
    private String sessionKey;
    private String userImage;

    public UserBean() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserDeviceIccid() {
        return userDeviceIccid;
    }

    public void setUserDeviceIccid(String userDeviceIccid) {
        this.userDeviceIccid = userDeviceIccid;
    }

    public Long getUserPhonenum() {
        return userPhonenum;
    }

    public void setUserPhonenum(Long userPhonenum) {
        this.userPhonenum = userPhonenum;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUserFishPondName() {
        return userFishPondName;
    }

    public void setUserFishPondName(String userFishPondName) {
        this.userFishPondName = userFishPondName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    public String getUserImage() {
        return userImage;
    }

    public void setUserImage(String userImage) {
        this.userImage = userImage;
    }

    @Override
    public String toString() {
        return "UserBean{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                ", userDeviceIccid='" + userDeviceIccid + '\'' +
                ", userPhonenum=" + userPhonenum +
                ", password='" + password + '\'' +
                ", createTime='" + createTime + '\'' +
                ", userFishPondName='" + userFishPondName + '\'' +
                ", sessionKey='" + sessionKey + '\'' +
                ", userImage='" + userImage + '\'' +
                '}';
    }
}
