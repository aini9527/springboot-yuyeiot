package com.example.websockettest.beans;

import javax.persistence.*;

@Entity
@Table(name = "iccid")
public class Iccid {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String iccid;

    public Iccid() {
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIccid() {
        return iccid;
    }

    public void setIccid(String iccid) {
        this.iccid = iccid;
    }

    @Override
    public String toString() {
        return "Iccid{" +
                "id=" + id +
                ", iccid='" + iccid + '\'' +
                '}';
    }
}
