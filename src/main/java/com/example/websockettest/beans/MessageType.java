package com.example.websockettest.beans;

public class MessageType {

    private int messageType;// 0:参数上传 1:状态上传 2:控制信息

    private ParameterBean parameterBean;

    private StatusMessage statusMessage;

    private ALarmBean01 aLarmBean01;

    public MessageType(int messageType) {
        this.messageType = messageType;
    }

    public int getMessageType() {
        return messageType;
    }

    public void setMessageType(int messageType) {
        this.messageType = messageType;
    }

    public ParameterBean getParameterBean() {
        return parameterBean;
    }

    public void setParameterBean(ParameterBean parameterBean) {
        this.parameterBean = parameterBean;
    }

    public StatusMessage getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(StatusMessage statusMessage) {
        this.statusMessage = statusMessage;
    }

    public ALarmBean01 getaLarmBean01() {
        return aLarmBean01;
    }

    public void setaLarmBean01(ALarmBean01 aLarmBean01) {
        this.aLarmBean01 = aLarmBean01;
    }

    @Override
    public String toString() {
        return "MessageType{" +
                "messageType=" + messageType +
                ", parameterBean=" + parameterBean +
                ", statusMessage=" + statusMessage +
                '}';
    }
}
