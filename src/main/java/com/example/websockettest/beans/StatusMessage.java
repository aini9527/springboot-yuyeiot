package com.example.websockettest.beans;

import javax.persistence.*;

@Entity
@Table(name = "StatusMessage")
public class StatusMessage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true)
    private String iccid;

    @Column(nullable = true)
    private Long user_num;

    @Column(nullable = true)
    private String deviceStatus;

    public StatusMessage() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIccid() {
        return iccid;
    }

    public void setIccid(String iccid) {
        this.iccid = iccid;
    }

    public Long getUser_num() {
        return user_num;
    }

    public void setUser_num(Long user_num) {
        this.user_num = user_num;
    }

    public String getDeviceStatus() {
        return deviceStatus;
    }

    public void setDeviceStatus(String deviceStatus) {
        this.deviceStatus = deviceStatus;
    }

    @Override
    public String toString() {
        return "StatusMessage{" +
                "id=" + id +
                ", iccid='" + iccid + '\'' +
                ", user_num=" + user_num +
                ", deviceStatus='" + deviceStatus + '\'' +
                '}';
    }
}
