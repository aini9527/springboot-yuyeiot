package com.example.websockettest.beans;

import javax.persistence.*;

@Entity
@Table(name = "TimingReCode")
public class TimingReCode {
    private String iccid;
    private String operationTime;
    private int controlBtnIndex;
    private int controlFlag;
    private int alarmFlag;
    private int alarmType;
    private Long dateForLong;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public String getOperationTime() {
        return operationTime;
    }

    public void setOperationTime(String operationTime) {
        this.operationTime = operationTime;
    }

    public int getControlBtnIndex() {
        return controlBtnIndex;
    }

    public void setControlBtnIndex(int controlBtnIndex) {
        this.controlBtnIndex = controlBtnIndex;
    }

    public int getControlFlag() {
        return controlFlag;
    }

    public void setControlFlag(int controlFlag) {
        this.controlFlag = controlFlag;
    }

    public int getAlarmFlag() {
        return alarmFlag;
    }

    public void setAlarmFlag(int alarmFlag) {
        this.alarmFlag = alarmFlag;
    }

    public int getAlarmType() {
        return alarmType;
    }

    public void setAlarmType(int alarmType) {
        this.alarmType = alarmType;
    }

    public Long getDateForLong() {
        return dateForLong;
    }

    public void setDateForLong(Long dateForLong) {
        this.dateForLong = dateForLong;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIccid() {
        return iccid;
    }

    public void setIccid(String iccid) {
        this.iccid = iccid;
    }

    @Override
    public String toString() {
        return "TimingReCode{" +
                "iccid='" + iccid + '\'' +
                ", operationTime='" + operationTime + '\'' +
                ", controlBtnIndex=" + controlBtnIndex +
                ", controlFlag=" + controlFlag +
                ", alarmFlag=" + alarmFlag +
                ", alarmType=" + alarmType +
                ", dateForLong=" + dateForLong +
                ", id=" + id +
                '}';
    }
}
