package com.example.websockettest.beans;

import javax.persistence.*;

@Entity
@Table(name = "ControlMessage")
public class ControlMessage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = true)
    private int MessageType;
    @Column(nullable = true)
    private String user_name;
    @Column(nullable = true)
    private Long user_num;
    @Column(nullable = true)
    private String control_content;
    @Column(nullable = true)
    private String iccid;

    private int controlBtnIndex;

    private int switchFlag;

    private String sessionKey;

    public ControlMessage() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getMessageType() {
        return MessageType;
    }

    public void setMessageType(int messageType) {
        MessageType = messageType;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public Long getUser_num() {
        return user_num;
    }

    public void setUser_num(Long user_num) {
        this.user_num = user_num;
    }

    public String getControl_content() {
        return control_content;
    }

    public void setControl_content(String control_content) {
        this.control_content = control_content;
    }

    public String getIccid() {
        return iccid;
    }

    public void setIccid(String iccid) {
        this.iccid = iccid;
    }

    public int getControlBtnIndex() {
        return controlBtnIndex;
    }

    public void setControlBtnIndex(int controlBtnIndex) {
        this.controlBtnIndex = controlBtnIndex;
    }

    public int getSwitchFlag() {
        return switchFlag;
    }

    public void setSwitchFlag(int switchFlag) {
        this.switchFlag = switchFlag;
    }

    public String getSessionKey() {
        return sessionKey;
    }

    public void setSessionKey(String sessionKey) {
        this.sessionKey = sessionKey;
    }

    @Override
    public String toString() {
        return "ControlMessage{" +
                "id=" + id +
                ", MessageType=" + MessageType +
                ", user_name='" + user_name + '\'' +
                ", user_num='" + user_num + '\'' +
                ", control_content='" + control_content + '\'' +
                ", iccid='" + iccid + '\'' +
                ", controlBtnIndex=" + controlBtnIndex +
                ", switchFlag=" + switchFlag +
                ", sessionKey='" + sessionKey + '\'' +
                '}';
    }
}
