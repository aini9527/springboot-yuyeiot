package com.example.websockettest.beans;

public class MiniProgramBean<T> {

    private int status;
    private String message;

    private T data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public class LoginResultFromWeChat{
        private String session_key;
        private String openid;
        private String userFishPondName;
        private String userDeviceIccid;
        private String userName;
        private String userImage;

        public String getSession_key() {
            return session_key;
        }

        public void setSession_key(String session_key) {
            this.session_key = session_key;
        }

        public String getOpenid() {
            return openid;
        }

        public void setOpenid(String openid) {
            this.openid = openid;
        }

        public String getUserFishPondName() {
            return userFishPondName;
        }

        public void setUserFishPondName(String userFishPondName) {
            this.userFishPondName = userFishPondName;
        }

        public String getUserDeviceIccid() {
            return userDeviceIccid;
        }

        public void setUserDeviceIccid(String userDeviceIccid) {
            this.userDeviceIccid = userDeviceIccid;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getUserImage() {
            return userImage;
        }

        public void setUserImage(String userImage) {
            this.userImage = userImage;
        }

        @Override
        public String toString() {
            return "LoginResultFromWeChat{" +
                    "session_key='" + session_key + '\'' +
                    ", openid='" + openid + '\'' +
                    ", userFishPondName='" + userFishPondName + '\'' +
                    ", userDeviceIccid='" + userDeviceIccid + '\'' +
                    ", userName='" + userName + '\'' +
                    ", userImage='" + userImage + '\'' +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "MiniProgramBean{" +
                "status=" + status +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
