package com.example.websockettest.controller;


import com.example.websockettest.beans.Iccid;
import com.example.websockettest.beans.StatusMessage;
import com.example.websockettest.databaseapi.StatusMessageRepository;
import com.example.websockettest.service.MainWebSocket;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@RestController
public class TestController {

    @Autowired
    private StatusMessageRepository statusMessageRepository;

    @RequestMapping(value = "/isLine/{iccid}")
    public String isLine(@PathVariable String iccid){
        if(MainWebSocket.webSocketMap.get(iccid) != null){
            return iccid + "在线!";
        }else {
            return iccid + "不在线!";
        }
    }

    @RequestMapping(value = "/sendMessage/{iccid}")
    public String sendMessage(@PathVariable String iccid, @Param("info") String info) throws IOException {
        MainWebSocket webSocketService = MainWebSocket.webSocketMap.get(iccid);
        if(webSocketService != null){
            synchronized (webSocketService.getSession()){
                webSocketService.getSession().getBasicRemote().sendText(info);
            }
            return "发送成功！ -->" + info;
        }else{
            return "设备不在线!";
        }
    }

    @PostMapping(value = "/postTest/{ykq}")
    public String postTest(@PathVariable String ykq,@RequestBody Iccid iccidOb){
        List<StatusMessage> statusMessages = statusMessageRepository.findAll();
        return iccidOb.toString();
    }
}
