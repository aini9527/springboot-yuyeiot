package com.example.websockettest.controller;

import com.example.websockettest.beans.*;
import com.example.websockettest.databaseapi.*;
import com.example.websockettest.service.MainWebSocket;
import com.example.websockettest.service.MyHttpUtils;
import com.example.websockettest.timing.QuartzScheduler;
import com.example.websockettest.utils.O;
import com.example.websockettest.utils.PhoneFormatCheckUtils;
import com.google.gson.Gson;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.example.websockettest.config.StatusCodeConfig.*;

@RestController
public class controlPageController {

    @Autowired
    private StatusMessageRepository statusMessageRepository;

    @Autowired
    private AlarmBean01Repository alarmBean01Repository;

    @Autowired
    private QuartzScheduler quartzScheduler;

    @Autowired
    private UserBeanRepository userBeanRepository;

    @Autowired
    private IcciddRepository icciddRepository;

    @Autowired
    private ParameterRepository parameterRepository;

    @Value(value = "${mini.appId}")
    private String appId;

    @Value(value = "${mini.secret}")
    private String secret;

    @RequestMapping("/getDeviceStatus/{iccid}")
    public String getDeviceStatus(@PathVariable String iccid){
        StatusMessage statusMessage = statusMessageRepository.findByIccid(iccid);
        if(statusMessage != null){
            return new Gson().toJson(statusMessage);
        }else{
            return new Gson().toJson(new StatusMessage());
        }
    }

    @GetMapping("/getDeviceStatus")
    public String getDeviceStatus(@Param("sessionKey") String sessionKey, @Param("phoneNum") Long phoneNum, @Param("iccid") String iccid){
        if(sessionKey == null || iccid == null || phoneNum == null){
            MiniProgramBean<MiniProgramBean.LoginResultFromWeChat> miniProgramBean = new MiniProgramBean<>();
            miniProgramBean.setStatus(GET_DEVICE_STATUS_PARAM_LOST);
            miniProgramBean.setMessage("getDeviceParams字段缺失");
            O.o("字段缺失");
            return new Gson().toJson(miniProgramBean);
        }
        if(sessionKey.equals("") || phoneNum == 0 || iccid.equals("")){
            MiniProgramBean<MiniProgramBean.LoginResultFromWeChat> miniProgramBean = new MiniProgramBean<>();
            miniProgramBean.setStatus(GET_DEVICE_STATUS_PARAM_ERROR);
            miniProgramBean.setMessage("getDeviceStatus字段错误");
            O.o("字段错误");
            return new Gson().toJson(miniProgramBean);
        }
        if(!icciddRepository.existsByIccid(iccid)){
            MiniProgramBean<MiniProgramBean.LoginResultFromWeChat> miniProgramBean = new MiniProgramBean<>();
            miniProgramBean.setStatus(GET_DEVICE_STATUS_ICCID_NOT_FOUND);
            miniProgramBean.setMessage("iccid不存在");
            O.o("iccid不存在");
            return new Gson().toJson(miniProgramBean);
        }
        if(!iccid.equals(userBeanRepository.findIccidByphoneNum(phoneNum))){
            MiniProgramBean<MiniProgramBean.LoginResultFromWeChat> miniProgramBean = new MiniProgramBean<>();
            miniProgramBean.setStatus(GET_DEVICE_STATUS_ICCID_USER_NOT_MATCH);
            miniProgramBean.setMessage("用户和iccid不匹配");
            O.o("用户和iccid不匹配");
            return new Gson().toJson(miniProgramBean);
        }
        if(userBeanRepository.findSessionKeyByphoneNum(phoneNum).equals(sessionKey)){
            if(isOnline(iccid)){
                StatusMessage statusMessage = statusMessageRepository.findByIccid(iccid);
                MiniProgramBean<StatusMessage> miniProgramBean = new MiniProgramBean<>();
                miniProgramBean.setStatus(GET_DEVICE_STATUS_SUCCESS);
                miniProgramBean.setMessage("设备状态获取成功");
                miniProgramBean.setData(statusMessage);
//                O.o("设备状态获取成功");
                return new Gson().toJson(miniProgramBean);
            }else{
                MiniProgramBean<MiniProgramBean.LoginResultFromWeChat> miniProgramBean = new MiniProgramBean<>();
                miniProgramBean.setStatus(GET_DEVICE_STATUS_DEVICE_NOT_ONLINE);
                miniProgramBean.setMessage("设备不在线");
                O.o("设备不在线");
                return new Gson().toJson(miniProgramBean);
            }

        }else{
            MiniProgramBean<MiniProgramBean.LoginResultFromWeChat> miniProgramBean = new MiniProgramBean<>();
            miniProgramBean.setStatus(GET_DEVICE_STATUS_SESSION_KET_NOT_MATCH);
            miniProgramBean.setMessage("sessionKey不匹配");
            O.o("sessionKey不匹配");
            return new Gson().toJson(miniProgramBean);
        }

    }

    @GetMapping("/getDeviceParams")
    public String getDeviceParams(@Param("sessionKey") String sessionKey, @Param("phoneNum") Long phoneNum, @Param("iccid") String iccid){
        if(sessionKey == null || phoneNum == null || iccid == null){
            MiniProgramBean<MiniProgramBean.LoginResultFromWeChat> miniProgramBean = new MiniProgramBean<>();
            miniProgramBean.setStatus(PARAM_WORD_LOST);
            miniProgramBean.setMessage("getDeviceParams字段缺失");
            O.o("字段缺失");
            return new Gson().toJson(miniProgramBean);
        }
        if(sessionKey.equals("") || phoneNum == 0 || iccid.equals("")){
            MiniProgramBean<MiniProgramBean.LoginResultFromWeChat> miniProgramBean = new MiniProgramBean<>();
            miniProgramBean.setStatus(PARAM_WORD_ERROR);
            miniProgramBean.setMessage("getDeviceParams字段错误");
            O.o("字段错误");
            return new Gson().toJson(miniProgramBean);
        }
        if(!icciddRepository.existsByIccid(iccid)){
            MiniProgramBean<MiniProgramBean.LoginResultFromWeChat> miniProgramBean = new MiniProgramBean<>();
            miniProgramBean.setStatus(PARAM_ICCID_NOT_FOUND);
            miniProgramBean.setMessage("iccid不存在");
            O.o("iccid不存在");
            return new Gson().toJson(miniProgramBean);
        }
        if(!iccid.equals(userBeanRepository.findIccidByphoneNum(phoneNum))){
            MiniProgramBean<MiniProgramBean.LoginResultFromWeChat> miniProgramBean = new MiniProgramBean<>();
            miniProgramBean.setStatus(PARAM_ICCID_PHONE_NOT_MATCH);
            miniProgramBean.setMessage("用户和iccid不匹配");
            O.o("用户和iccid不匹配");
            return new Gson().toJson(miniProgramBean);
        }
        if(userBeanRepository.findSessionKeyByphoneNum(phoneNum).equals(sessionKey)){
            ParameterBean parameterBean = parameterRepository.findByIccid(iccid);
            if(parameterBean != null){
                MiniProgramBean<ParameterBean> miniProgramBean = new MiniProgramBean<>();
                miniProgramBean.setStatus(PARAM_GET_SUCCESS);
                miniProgramBean.setMessage("获取设备参数成功！");
                miniProgramBean.setData(parameterBean);
                O.o("获取设备参数成功！");
                return new Gson().toJson(miniProgramBean);
            }else{
                MiniProgramBean<MiniProgramBean.LoginResultFromWeChat> miniProgramBean = new MiniProgramBean<>();
                miniProgramBean.setStatus(PARAM_ICCID_DEVICE_NULL);
                miniProgramBean.setMessage("设备参数为空！");
                O.o("设备参数为空！");
                return new Gson().toJson(miniProgramBean);
            }
        }else{
            MiniProgramBean<MiniProgramBean.LoginResultFromWeChat> miniProgramBean = new MiniProgramBean<>();
            miniProgramBean.setStatus(PARAM_SESSION_KEY_NOT_MATCH);
            miniProgramBean.setMessage("sessionKey不匹配");
            O.o("sessionKey不匹配");
            return new Gson().toJson(miniProgramBean);
        }
    }

    @RequestMapping("/getAlarms")
    List<ALarmBean01> getAlarms(@Param("iccid") String iccid) throws SchedulerException {
        List<ALarmBean01> alarmBean01s = alarmBean01Repository.findByIccid(iccid);
        List<ALarmBean01> res = new ArrayList<>();
        String re;
        for(ALarmBean01 alarmBean01 : alarmBean01s){
            re = null;
            re = quartzScheduler.getJobInfo(alarmBean01.getIccid() + "==" + alarmBean01.getDateForLong() + "==" + alarmBean01.getAlarmFlag());
            if(re != null && !"".equals(re) && re.equals("NORMAL")){
                res.add(alarmBean01);
            }
        }
        return res;
    }

    @GetMapping("/getAlarmsFor")
    List<String> getAlarmsFor(@Param("iccid") String iccid) throws SchedulerException {
        List<ALarmBean01> alarmBean01s = alarmBean01Repository.findByIccid(iccid);
        List<String> res = new ArrayList<>();
        String re;
        for(ALarmBean01 alarmBean01 : alarmBean01s){
            re = null;
            re = quartzScheduler.getJobInfoFor(alarmBean01.getIccid() + "==" + alarmBean01.getDateForLong() + "==" + alarmBean01.getAlarmFlag());
            res.add(re);
        }
        return res;
    }

    @PostMapping(value = "/deleteAlarms")
    String deleteAlarms(@RequestBody List<ALarmBean01> aLarmBean01s) throws SchedulerException {
        if(alarmBean01Repository != null){
            alarmBean01Repository.deleteAll(aLarmBean01s);
            for(ALarmBean01 aLarmBean01 : aLarmBean01s){
                quartzScheduler.deleteJob(aLarmBean01.getIccid() + "==" + aLarmBean01.getDateForLong() + "==" + aLarmBean01.getAlarmFlag());
            }

            return "0";
        }else{
            return "1";
        }

    }

    @GetMapping("miniprogram/login")
    public String login(@Param("code") String code, @Param("phoneNum") long phoneNum, @Param("passwordValue") String passwordValue) throws IOException {
        O.o("小程序登录：");
        UserBean userBean;
        userBean = userBeanRepository.findByUserPhonenum(phoneNum);
//        O.o(userBean.toString() + "   " + passwordValue);
        if(userBean != null){
            if(userBean.getUserPhonenum() == phoneNum && userBean.getPassword().equals(passwordValue)){
                String re = MyHttpUtils.doGet("https://api.weixin.qq.com/sns/jscode2session?appid=" + appId + "&secret=" + secret + "&js_code=" + code + "&grant_type=authorization_code");
                MiniProgramBean.LoginResultFromWeChat resultFromWeChat = new Gson().fromJson(re, MiniProgramBean.LoginResultFromWeChat.class);
                resultFromWeChat.setUserDeviceIccid(userBean.getUserDeviceIccid());
                resultFromWeChat.setUserName(userBean.getUserName());
                resultFromWeChat.setUserFishPondName(userBean.getUserFishPondName());
                resultFromWeChat.setUserImage(userBean.getUserImage());
                MiniProgramBean<MiniProgramBean.LoginResultFromWeChat> miniProgramBean = new MiniProgramBean<>();
                miniProgramBean.setStatus(LOGIN_SUCCESS);
                miniProgramBean.setMessage("ok");
                miniProgramBean.setData(resultFromWeChat);

                userBeanRepository.updateByUserPhonenum(resultFromWeChat.getSession_key(), phoneNum);

                return new Gson().toJson(miniProgramBean);
            }else{
                //用户名和密码不匹配
                MiniProgramBean<MiniProgramBean.LoginResultFromWeChat> miniProgramBean = new MiniProgramBean<>();
                miniProgramBean.setStatus(LOGON_USERNAME_PASSWORD_ERROR);
                miniProgramBean.setMessage("用户名和密码不匹配");
                return new Gson().toJson(miniProgramBean);
            }
        }else{
            //无该用户
            MiniProgramBean<MiniProgramBean.LoginResultFromWeChat> miniProgramBean = new MiniProgramBean<>();
            miniProgramBean.setStatus(LOGON_NO_USER);
            miniProgramBean.setMessage("无该用户！");
            return new Gson().toJson(miniProgramBean);
        }
    }

    @GetMapping("/deleteAccount")
    public String deleteAccount(@Param("phoneNum") Long phoneNum, @Param("superMasterName") String superMasterName){
        if(superMasterName.equals("ykq")){
            if(userBeanRepository.existsByUserPhonenum(phoneNum)){
                userBeanRepository.deleteByUserPhonenum(phoneNum);
                MiniProgramBean<MiniProgramBean.LoginResultFromWeChat> miniProgramBean = new MiniProgramBean<>();
                miniProgramBean.setStatus(DELETE_ACCOUNT_SUCCESS);
                miniProgramBean.setMessage("用户：" + phoneNum + "，注销成功！");
                O.o("用户：" + phoneNum + "，注销成功！");
                return new Gson().toJson(miniProgramBean);
            }else{
                MiniProgramBean<MiniProgramBean.LoginResultFromWeChat> miniProgramBean = new MiniProgramBean<>();
                miniProgramBean.setStatus(DELETE_ACCOUNT_FAIL);
                miniProgramBean.setMessage("用户：" + phoneNum + "，注销失败，无该用户！");
                O.o("用户：" + phoneNum + "，注销失败，无该用户！");
                return new Gson().toJson(miniProgramBean);
            }

        }else{
            MiniProgramBean<MiniProgramBean.LoginResultFromWeChat> miniProgramBean = new MiniProgramBean<>();
            miniProgramBean.setStatus(DELETE_ACCOUNT_FAIL);
            miniProgramBean.setMessage("用户：" + phoneNum + "，注销失败，" + superMasterName + "没有权限！");
            O.o("用户：" + phoneNum + "，注销失败，" + superMasterName + "没有权限！");
            return new Gson().toJson(miniProgramBean);
        }
    }

    @PostMapping("/miniRegister")
    public String miniRegister(@RequestBody MiniRegisterBean miniRegisterBean){
        if(miniRegisterBean.getPhoneNum() == null || miniRegisterBean.getPassword() == null || miniRegisterBean.getUserName() == null){
            MiniProgramBean<MiniProgramBean.LoginResultFromWeChat> miniProgramBean = new MiniProgramBean<>();
            miniProgramBean.setStatus(REGISTER_FAIL);
            miniProgramBean.setMessage("注册失败！");
            O.o("注册失败！字段缺失！");
            return new Gson().toJson(miniProgramBean);
        }
        if(miniRegisterBean != null){
            if(PhoneFormatCheckUtils.isChinaPhoneLegal(miniRegisterBean.getPhoneNum().toString())){
                if(userBeanRepository.existsByUserPhonenum(miniRegisterBean.getPhoneNum())){
                    MiniProgramBean<MiniProgramBean.LoginResultFromWeChat> miniProgramBean = new MiniProgramBean<>();
                    miniProgramBean.setStatus(REGISTER_FAIL_PHONE_NUM_ED);
                    miniProgramBean.setMessage("手机号已注册");
                    O.o("注册失败，手机号已经注册！");
                    return new Gson().toJson(miniProgramBean);
                }
                if(miniRegisterBean.getIccid() != null && !miniRegisterBean.getIccid().equals("") && !icciddRepository.existsByIccid(miniRegisterBean.getIccid())){
                    MiniProgramBean<MiniProgramBean.LoginResultFromWeChat> miniProgramBean = new MiniProgramBean<>();
                    miniProgramBean.setStatus(REGISTER_FAIL_ICCID_ERROR);
                    miniProgramBean.setMessage("注册失败！iccid错误！");
                    O.o("注册失败！iccid错误！");
                    return new Gson().toJson(miniProgramBean);
                }
                if(miniRegisterBean.getIccid() != null && !miniRegisterBean.getIccid().equals("") && userBeanRepository.existsByUserDeviceIccid(miniRegisterBean.getIccid())){
                    MiniProgramBean<MiniProgramBean.LoginResultFromWeChat> miniProgramBean = new MiniProgramBean<>();
                    miniProgramBean.setStatus(REGISTER_FAIL_ICCID_BIND);
                    miniProgramBean.setMessage("注册失败！iccid已被绑定！");
                    O.o("注册失败！iccid已被绑定！");
                    return new Gson().toJson(miniProgramBean);
                }
                UserBean userBean = new UserBean();
                userBean.setUserName(miniRegisterBean.getUserName());
                userBean.setUserPhonenum(miniRegisterBean.getPhoneNum());
                userBean.setPassword(miniRegisterBean.getPassword());
                userBean.setUserDeviceIccid(miniRegisterBean.getIccid());
                userBean.setUserImage(miniRegisterBean.getUserImage());
                SimpleDateFormat sp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                Date date = new Date();
                date.setTime(System.currentTimeMillis());
                userBean.setCreateTime(sp.format(date));
                userBeanRepository.save(userBean);
                MiniProgramBean<MiniProgramBean.LoginResultFromWeChat> miniProgramBean = new MiniProgramBean<>();
                miniProgramBean.setStatus(REGISTER_SUCCESS);
                miniProgramBean.setMessage("注册成功！");
                O.o("注册成功！");
                return new Gson().toJson(miniProgramBean);
            }else{
                MiniProgramBean<MiniProgramBean.LoginResultFromWeChat> miniProgramBean = new MiniProgramBean<>();
                miniProgramBean.setStatus(REGISTER_FAIL_PHONE_FORMAT_ERROR);
                miniProgramBean.setMessage("手机格式错误");
                O.o("注册失败，手机格式错误！");
                return new Gson().toJson(miniProgramBean);
            }
        }else{
            MiniProgramBean<MiniProgramBean.LoginResultFromWeChat> miniProgramBean = new MiniProgramBean<>();
            miniProgramBean.setStatus(REGISTER_FAIL);
            miniProgramBean.setMessage("注册失败！");
            O.o("注册失败，bean对象为空！");
            return new Gson().toJson(miniProgramBean);
        }

    }

    @RequestMapping(value = "/isLineY/{iccid}")
    public String isLine(@PathVariable String iccid){
        if(MainWebSocket.webSocketMap.get(iccid) != null){
            return iccid + "在线!";
        }else {
            return iccid + "不在线!";
        }
    }
    public boolean isOnline(String iccid){
        if(MainWebSocket.webSocketMap.get(iccid) != null){
            return true;
        }else {
            return false;
        }
    }

    @GetMapping("/bindDevice")
    public String bindDevice(@Param("session_key") String session_key, @Param("phoneNum") Long phoneNum, @Param("iccid") String iccid){
        if(session_key == null || phoneNum == null || iccid == null){
            MiniProgramBean<MiniProgramBean.LoginResultFromWeChat> miniProgramBean = new MiniProgramBean<>();
            miniProgramBean.setStatus(BIND_PARAM_LOST);
            miniProgramBean.setMessage("字段缺失");
            O.o("字段缺失");
            return new Gson().toJson(miniProgramBean);
        }
        if(session_key.equals("") || phoneNum == 0 || iccid.equals("")){
            MiniProgramBean<MiniProgramBean.LoginResultFromWeChat> miniProgramBean = new MiniProgramBean<>();
            miniProgramBean.setStatus(BIND_PARAM_ERROR);
            miniProgramBean.setMessage("字段错误");
            O.o("字段错误");
            return new Gson().toJson(miniProgramBean);
        }
        if(!"".equals(userBeanRepository.findIccidByphoneNum(phoneNum)) && userBeanRepository.findIccidByphoneNum(phoneNum) != null){
            MiniProgramBean<MiniProgramBean.LoginResultFromWeChat> miniProgramBean = new MiniProgramBean<>();
            miniProgramBean.setStatus(BIND_YOU_HAD_BIND);
            miniProgramBean.setMessage("你已经绑定");
            O.o("你已经绑定!"+ userBeanRepository.findIccidByphoneNum(phoneNum));
            return new Gson().toJson(miniProgramBean);
        }
        if(userBeanRepository.existsByUserDeviceIccid(iccid)){
            MiniProgramBean<MiniProgramBean.LoginResultFromWeChat> miniProgramBean = new MiniProgramBean<>();
            miniProgramBean.setStatus(BIND_ICCID_HAD_BIND);
            miniProgramBean.setMessage("该iccid已经被人绑定");
            O.o("该iccid已经被人绑定");
            return new Gson().toJson(miniProgramBean);
        }
        if(userBeanRepository.findSessionKeyByphoneNum(phoneNum).equals(session_key)){
            userBeanRepository.addDevice(iccid, phoneNum);
            MiniProgramBean<MiniProgramBean.LoginResultFromWeChat> miniProgramBean = new MiniProgramBean<>();
            miniProgramBean.setStatus(BIND_SUCCESS);
            miniProgramBean.setMessage("绑定成功！");
            O.o("绑定成功！");
            return new Gson().toJson(miniProgramBean);
        }else{
            MiniProgramBean<MiniProgramBean.LoginResultFromWeChat> miniProgramBean = new MiniProgramBean<>();
            miniProgramBean.setStatus(BIND_SESSION_KEY_NOT_MATCH);
            miniProgramBean.setMessage("sessionKey和手机号不匹配");
            O.o("sessionKey和手机号不匹配");
            return new Gson().toJson(miniProgramBean);
        }
    }

    @GetMapping("/unBindDevice")
    public String unBindDevice(@Param("sessionKey") String sessionKey, @Param("phoneNum") Long phoneNum, @Param("iccid") String iccid){
        return "";
    }

    @PostMapping(value = "/sendMessageY")
    public String sendMessage(@RequestBody ControlMessage controlMessage) throws IOException {

        MainWebSocket webSocketService = MainWebSocket.webSocketMap.get(controlMessage.getIccid());
        if(userBeanRepository.findIccidByphoneNum(controlMessage.getUser_num()).equals(controlMessage.getIccid())){
            if(webSocketService != null){
                if(controlMessage.getSessionKey() != null){
                    if(controlMessage.getSessionKey().equals(userBeanRepository.findSessionKeyByphoneNum(controlMessage.getUser_num()))){
                        synchronized (webSocketService.getSession()){
                            webSocketService.getSession().getBasicRemote().sendText(new Gson().toJson(controlMessage));
                            O.o("发送成功！ -->" + controlMessage.toString());
                            return "ok";
                        }
                    }else {
                        return "SessionKey错误";
                    }
                }else{
                    return "SessionKey为空";
                }
            }else{
                return "设备不在线!";
            }
        }else{
            return "用户和iccid不匹配！";
        }
    }

}
