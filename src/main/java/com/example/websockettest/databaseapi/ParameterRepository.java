package com.example.websockettest.databaseapi;

import com.example.websockettest.beans.ParameterBean;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface ParameterRepository extends JpaRepository<ParameterBean, Long> {
    ParameterBean findByIccid(String iccid);

    @Transactional
    @Modifying // QueryExecutionRequestException: Not supported for DML operations
    @Query("update ParameterBean set controlBtnNames=?1 where iccid=?2")
    void updateControlBtnNamesByIccid(String controlBtnNames,String iccid);

    @Transactional
    @Modifying // QueryExecutionRequestException: Not supported for DML operations
    @Query("update ParameterBean set fishPondName=?1 where iccid=?2")
    void updateFishPondNameByIccid(String fishPondName,String iccid);

    @Transactional
    @Modifying // QueryExecutionRequestException: Not supported for DML operations
    @Query("update ParameterBean set controlBtnNames=?1, fishPondName=?2 where iccid=?3")
    void updateControlBtnNamesAndFishPondNameByIccid(String controlBtnNames, String fishPondName, String iccid);


}
