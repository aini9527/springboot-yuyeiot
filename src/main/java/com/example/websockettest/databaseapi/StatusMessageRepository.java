package com.example.websockettest.databaseapi;

import com.example.websockettest.beans.StatusMessage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface StatusMessageRepository extends JpaRepository<StatusMessage, Long> {

//    @Query("from StatusMessage u where u.iccid=:iccid")
//    StatusMessage findStatusMessage(@Param("iccid") String videoTitle);

    boolean existsByIccid(String iccid);

    StatusMessage findByIccid(String iccid);

    @Transactional
    @Modifying // QueryExecutionRequestException: Not supported for DML operations
    @Query("update StatusMessage set deviceStatus=?1 where iccid=?2")
    void updateByIccid(String deviceStatus, String iccid);

    @Transactional
    @Modifying // QueryExecutionRequestException: Not supported for DML operations
    @Query("delete from StatusMessage where iccid=?1")
    void deleteAllByIccid(String iccid);
}
