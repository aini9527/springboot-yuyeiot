package com.example.websockettest.databaseapi;

import com.example.websockettest.beans.ALarmBean01;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface AlarmBean01Repository extends JpaRepository<ALarmBean01, Long> {

    @Transactional
    @Modifying // QueryExecutionRequestException: Not supported for DML operations
    @Query("delete from ALarmBean01 where alarmFlag=?1")
    void deleteAllByAlarmFlag(int alarmFlag);

    List<ALarmBean01> findByIccid(String iccid);

    void deleteAllByIccid(String iccid);
}
