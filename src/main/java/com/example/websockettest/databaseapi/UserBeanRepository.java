package com.example.websockettest.databaseapi;

import com.example.websockettest.beans.UserBean;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface UserBeanRepository extends JpaRepository<UserBean, Long> {

    UserBean findByUserPhonenum(long userPhonenum);

    @Transactional
    @Modifying // QueryExecutionRequestException: Not supported for DML operations
    @Query("update UserBean set sessionKey=?1 where userPhonenum=?2")
    void updateByUserPhonenum(String sessionKey, Long userPhonenum);

    @Transactional
    @Modifying // QueryExecutionRequestException: Not supported for DML operations
    @Query("update UserBean set userDeviceIccid=?1 where userPhonenum=?2")
    void addDevice(String userDeviceIccid, Long userPhonenum);

    @Transactional
    @Query("select sessionKey from UserBean where userPhonenum=?1")
    String findSessionKeyByphoneNum(long phoneNum);

    boolean existsByUserPhonenum(Long userPhoneNum);
    boolean existsByUserDeviceIccid(String UserDeviceIccid);

    @Transactional
    @Modifying // QueryExecutionRequestException: Not supported for DML operations
    void deleteByUserPhonenum(Long userPhoneNum);

    @Transactional
    @Query("select userDeviceIccid from UserBean where userPhonenum=?1")
    String findIccidByphoneNum(long userPhonenum);
}
