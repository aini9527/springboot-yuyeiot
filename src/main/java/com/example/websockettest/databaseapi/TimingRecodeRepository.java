package com.example.websockettest.databaseapi;


import com.example.websockettest.beans.TimingReCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
public interface TimingRecodeRepository extends JpaRepository<TimingReCode, Long> {
    @Transactional
    @Modifying // QueryExecutionRequestException: Not supported for DML operations
    @Query("delete from TimingReCode where iccid=?1")
    void deleteAllByIccid(String iccid);
}
