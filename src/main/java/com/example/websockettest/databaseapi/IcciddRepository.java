package com.example.websockettest.databaseapi;

import com.example.websockettest.beans.Iccid;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IcciddRepository extends JpaRepository<Iccid, Long> {

    boolean existsByIccid(String iccid);
}
