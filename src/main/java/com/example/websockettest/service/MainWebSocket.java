package com.example.websockettest.service;

import com.example.websockettest.beans.ALarmBean01;
import com.example.websockettest.beans.Iccid;
import com.example.websockettest.beans.MessageType;
import com.example.websockettest.beans.ParameterBean;
import com.example.websockettest.databaseapi.IcciddRepository;
import com.example.websockettest.databaseapi.ParameterRepository;
import com.example.websockettest.databaseapi.StatusMessageRepository;
import com.example.websockettest.utils.MyUtils;
import com.example.websockettest.utils.O;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.LongAdder;

/**
 * websocket服务端核心类
 * @author : Kagou_Y
 * @date : 2020-08-06
 */
@ServerEndpoint("/websocket/{iccid}")
@Component
public class MainWebSocket {

    @Autowired
    private StatusMessageRepository statusMessageRepository;

    @Autowired
    private IcciddRepository icciddRepository;

    @Autowired
    private ParameterRepository parameterRepository;

    @Autowired
    private WebSocketService webSocketService;

    /**
     * 记录当前websocket的连接数（保证线程安全）
     */
    private static LongAdder connectAccount = new LongAdder();

//    /**
//     *存放每个客户端对应的websocketServer对象(需保证线程安全)
//     */
//    private static CopyOnWriteArraySet<WebSocketService> webSocketSet = new CopyOnWriteArraySet<>();

    public static Map<String, MainWebSocket> webSocketMap = new ConcurrentHashMap<>();
    /**
     * 与客户端的连接对象
     */
    private Session session;

    //连接服务器webSocket的设备终端唯一标识
    private String iccid;

    private boolean isValidated;


    //为了使Autowired标注的字段不为空
    public static MainWebSocket MainWebSocket;
    @PostConstruct
    public void init() {
        MainWebSocket = this;
    }


    /**
     * 连接成功调用的方法
     * @param session
     */
    @OnOpen
    public void onOpen(Session session, @PathParam("iccid") String iccid ) throws IOException {
        this.session = session;

        connectAccount.increment();

        //iccid验证
        List<Iccid> iccids = MainWebSocket.icciddRepository.findAll();
        O.o("当前设备iccid为：" + iccid);
        this.iccid = iccid;
        if(iccids != null){
            for(int i = 0; i < iccids.size(); i++){
                O.o("iccid" + iccids.get(i).getIccid());
                if(iccids.get(i).getIccid().equals(iccid)){//验证成功
                    O.o("有新的连接接入，当前连接数为{}" + connectAccount.sum());
                    isValidated = false;
                    webSocketMap.put(iccid, this);
                    return;
                }
            }
            O.o("iccid验证失败!");
            isValidated = false;
            session.close();
        }else{
            O.o("读取数据库出错!");
        }

    }

    /**
     * 连接关闭时调用
     */
    @OnClose
    public void onClose() {
//        webSocketSet.remove(this);
        MainWebSocket.statusMessageRepository.deleteAllByIccid(this.iccid);
        webSocketMap.remove(this.iccid);
        connectAccount.decrement();
        O.o("有连接关闭，当前连接数为{}" + connectAccount.sum());
    }

    /**
     * 收到客户端消息时调用
     * @param message
     */
    @OnMessage
    public void onMessage(String message) {
        O.o("message为：" + message);
        if(MyUtils.validate(message)){
            MessageType messageType = MyUtils.string2ObjForGson(message, MessageType.class);
            O.o("MessageType：" + messageType.toString());
            //0为参数上传
            if(messageType.getMessageType() == 0){
                //...
                ParameterBean parameterBean = messageType.getParameterBean();
                parameterBean.setId(null);

                if(MainWebSocket.parameterRepository.findByIccid(parameterBean.getIccid()) != null){
                    //更新
                    O.o("更新操作");
                    if(parameterBean.getControlBtnNames() != null && parameterBean.getFishPondName() == null){
                        MainWebSocket.parameterRepository.updateControlBtnNamesByIccid(parameterBean.getControlBtnNames(), parameterBean.getIccid());
                    }else if(parameterBean.getFishPondName() != null && parameterBean.getControlBtnNames() == null){
                        MainWebSocket.parameterRepository.updateFishPondNameByIccid(parameterBean.getFishPondName(), parameterBean.getIccid());
                    }else if(parameterBean.getControlBtnNames() != null && parameterBean.getFishPondName() != null){
                        //都不为null
                        MainWebSocket.parameterRepository.updateControlBtnNamesAndFishPondNameByIccid(parameterBean.getControlBtnNames(), parameterBean.getFishPondName(), parameterBean.getIccid());
                    }else{
                        //都为null 这种情况属于异常情况
                    }
                }else{
                    MainWebSocket.parameterRepository.save(parameterBean);
                }
            }
            //1为状态上传
            else if(messageType.getMessageType() == 1){
                //...
                O.o("收到客户端发来的消息,message -> {}" + messageType.getStatusMessage().toString());
                if(MainWebSocket.statusMessageRepository.existsByIccid(messageType.getStatusMessage().getIccid())){
                    MainWebSocket.statusMessageRepository.updateByIccid(messageType.getStatusMessage().getDeviceStatus(), messageType.getStatusMessage().getIccid());
                }else{
                    MainWebSocket.statusMessageRepository.save(messageType.getStatusMessage());
                }
                sendMessage("收到你发过来的消息" + messageType.getStatusMessage().toString());
            }
            //2为定时闹钟上传
            else if(messageType.getMessageType() == 2){
                O.o("设置定时");
                ALarmBean01 aLarmBean01 = messageType.getaLarmBean01();
                try {
                    MainWebSocket.webSocketService.addAlarm(aLarmBean01);
                } catch (SchedulerException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    private void sendMessage(String message){
        if(!isValidated){
            return;
        }
        try {
            O.o("向" + iccid + "终端设备发送消息 message={}" + message);
            this.session.getBasicRemote().sendText(message);
        } catch (IOException e) {
            O.o("向客户端发送消息失败, {}" + e.getMessage());
            e.printStackTrace();
        }
    }

    public Session getSession() {
        return session;
    }

    public String getIccid() {
        return iccid;
    }

    /**
     * 服务端向所有客户端发送消息
     * @param message
     */
    public void sendMessageAll(String message) {
//        synchronized (webSocketSet) {
//            if (webSocketSet.isEmpty()){
//                return;
//            }
//            for (MainWebSocket MainWebSocket : webSocketSet) {
//                try {
//                    O.o("【websocket消息】 广播消息, message={}" + message);
//                    MainWebSocket.session.getBasicRemote().sendText(message);
//                } catch (IOException e) {
//                    O.o("向客户端发送消息失败, {}" + e.getMessage());
//                    e.printStackTrace();
//                    continue;
//                }
//            }
//        }
    }

}

