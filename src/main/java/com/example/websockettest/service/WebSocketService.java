package com.example.websockettest.service;


import com.example.websockettest.beans.ALarmBean01;
import com.example.websockettest.databaseapi.AlarmBean01Repository;
import com.example.websockettest.timing.QuartzScheduler;
import com.example.websockettest.utils.O;
import org.quartz.SchedulerException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
public class WebSocketService{

    @Autowired
    private AlarmBean01Repository alarmBean01Repository;

    @Autowired
    private QuartzScheduler quartzScheduler;

    @Async("scorePoolTaskExecutor")
    public synchronized void addAlarm(ALarmBean01 aLarmBean01) throws SchedulerException {
        if(aLarmBean01 == null){
            return;
        }
        Date date = new Date();
        date.setTime(aLarmBean01.getDateForLong());
        O.o("设置定时  / " + aLarmBean01.getIccid() + "  /  " + date.toString() + "  /  " + aLarmBean01.getControlBtnIndex() + "-" + aLarmBean01.getSwitchFlag());


        //设置定时

        if(quartzScheduler != null){
            quartzScheduler.addJob("com.example.websockettest.timing.ClockTask", aLarmBean01.getIccid() + "==" + aLarmBean01.getDateForLong() + "==" + aLarmBean01.getAlarmFlag(), aLarmBean01);
        }else{
            O.o("quartzScheduler为null");
        }
    }
}