package com.example.websockettest.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTimeUtil {

    public static final long ONE_DAY_M = 60*60*24*1000;
    public static final long ONE_DAY_S = 60*60*24;

    public static final String TIME_FORMAT_YEAS_MOTH_DAT = "yyyy-MM-dd";

    /**
     * 获取时间戳的年-月-日 hh:mm:ss.sss
     * @param timeStamp 时间戳
     * @param TimeFormat 时间格式
     * @return 按照输入格式生成的时间字符串
     */

    public static String getTimeFormatStringFromTimeStamp(long timeStamp, String TimeFormat){
        String result;
        Date date = new Date();
        date.setTime(timeStamp);
        SimpleDateFormat sf = new SimpleDateFormat(TimeFormat);
        result = sf.format(date);
        return result;
    }

    /**
     *获取时间戳
     * @param dateFormatString * yyyy-MM-dd...格式的时间字符串
     * @return 时间戳
     */

    public static long getTimeStampFromDateFormatString(String dateFormatString){
        long timeStamp = 0;
        SimpleDateFormat sf = new SimpleDateFormat(TIME_FORMAT_YEAS_MOTH_DAT);
        if(dateFormatString != null){
            try {
                Date date = sf.parse(dateFormatString);
                timeStamp = date.getTime();
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return timeStamp;
    }
}
