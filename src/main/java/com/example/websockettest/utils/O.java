package com.example.websockettest.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class O {

    private static boolean isTest;
    @Value("${isTest}")
    private void setIsTest(boolean tag){
        isTest = tag;
    }


    public static void ono(Object o){
        if (isTest) {
            System.out.print(o);
        }

    }

    public static void o(Object o){
        if (isTest) {
            System.out.println(o);
        }
    }
}
