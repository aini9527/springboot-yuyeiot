package com.example.websockettest.timing;

import java.util.Date;
import java.util.List;

import com.example.websockettest.beans.ALarmBean01;
import com.example.websockettest.databaseapi.AlarmBean01Repository;
import com.example.websockettest.utils.DateTimeUtil;
import com.example.websockettest.utils.O;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

/**
 * 任务调度
 *
 */
@Configuration
public class QuartzScheduler {

    @Autowired
    public Scheduler scheduler;

    @Autowired
    private AlarmBean01Repository alarmBean01Repository;


    /**
     * 获取Job信息
     *
     * @param name 类名
     * @return
     * @throws SchedulerException
     */
    public String getJobInfo(String name) throws SchedulerException {
        TriggerKey triggerKey = new TriggerKey(name, scheduler.DEFAULT_GROUP);
        SimpleTrigger cronTrigger = (SimpleTrigger) scheduler.getTrigger(triggerKey);
        if(cronTrigger != null){
            return scheduler.getTriggerState(triggerKey).name();
        }else{
            return "null";
        }

    }

    /**
     * 获取Job信息
     *
     * @param name 类名
     * @return
     * @throws SchedulerException
     */
    public String getJobInfoFor(String name) throws SchedulerException {
        TriggerKey triggerKey = new TriggerKey(name, scheduler.DEFAULT_GROUP);
        SimpleTrigger cronTrigger = (SimpleTrigger) scheduler.getTrigger(triggerKey);
        if(cronTrigger != null){
            return cronTrigger.getJobKey().getName() + "-->" + scheduler.getTriggerState(triggerKey).name();
        }else{
            return "null";
        }

    }

//    /**
//     * 修改某个任务的执行时间
//     *
//     * @param name 类名
//     * @param time 任务触发时间
//     * @return
//     * @throws SchedulerException
//     */
//    public boolean modifyJob(String name, String time) throws SchedulerException {
//        Date date = null;
//        TriggerKey triggerKey = new TriggerKey(name, scheduler.DEFAULT_GROUP);
//        CronTrigger cronTrigger = (CronTrigger) scheduler.getTrigger(triggerKey);
//        String oldTime = cronTrigger.getCronExpression();
//        if (!oldTime.equalsIgnoreCase(time)) {
//            CronScheduleBuilder cronScheduleBuilder = CronScheduleBuilder.cronSchedule(time);
//            CronTrigger trigger = TriggerBuilder.newTrigger().withIdentity(name, scheduler.DEFAULT_GROUP)
//                    .withSchedule(cronScheduleBuilder).build();
//            date = scheduler.rescheduleJob(triggerKey, trigger);
//        }
//        return date != null;
//    }

    /**
     * 暂停所有任务
     *
     * @throws SchedulerException
     */
    public void pauseAllJob() throws SchedulerException {
        scheduler.pauseAll();
    }

    /**
     * 暂停某个任务
     *
     * @param name 类名
     */
    public void pauseJob(String name) throws SchedulerException {
        JobKey jobKey = new JobKey(name, scheduler.DEFAULT_GROUP);
        JobDetail jobDetail = scheduler.getJobDetail(jobKey);
        if (jobDetail == null)
            return;
        scheduler.pauseJob(jobKey);
    }

    /**
     * 恢复所有任务
     *
     * @throws SchedulerException
     */
    public void resumeAllJob() throws SchedulerException {
        scheduler.resumeAll();
    }

    /**
     * 恢复某个任务
     *
     * @param name  类名
     */
    public void resumeJob(String name) throws SchedulerException {
        JobKey jobKey = new JobKey(name, scheduler.DEFAULT_GROUP);
        JobDetail jobDetail = scheduler.getJobDetail(jobKey);
        if (jobDetail == null)
            return;
        scheduler.resumeJob(jobKey);
    }

    /**
     * 删除某个任务
     *
     * @param name 类名
     */
    public void deleteJob(String name) throws SchedulerException {
        JobKey jobKey = new JobKey(name, scheduler.DEFAULT_GROUP);
        JobDetail jobDetail = scheduler.getJobDetail(jobKey);
        if (jobDetail == null)
            return;
        scheduler.deleteJob(jobKey);
    }

    /**
     * 添加定时器
     *
     * @param name 类名
     * @param aLarmBean01 任务信息
     * @throws SchedulerException
     */
    public void addJob(String className, String name, ALarmBean01 aLarmBean01) throws SchedulerException {
        Class classs = null;
        SimpleTrigger trigger;
        try {
            classs = Class.forName(className);
        } catch (ClassNotFoundException e1) {
            e1.printStackTrace();
        }
        JobDetail jobDetail = JobBuilder.newJob(classs).withIdentity(name, scheduler.DEFAULT_GROUP).build();
        jobDetail.getJobDataMap().put("Iccid",aLarmBean01.getIccid());
        jobDetail.getJobDataMap().put("ControlBtnIndex",aLarmBean01.getControlBtnIndex());
        jobDetail.getJobDataMap().put("SwitchFlag",aLarmBean01.getSwitchFlag());
        jobDetail.getJobDataMap().put("AlarmType", aLarmBean01.getAlarmType());
        jobDetail.getJobDataMap().put("AlarmFlag", aLarmBean01.getAlarmFlag());
        jobDetail.getJobDataMap().put("dateForLong", aLarmBean01.getDateForLong());

        //时间过了，马上执行bug TODO

        if(aLarmBean01.getAlarmType() == 1){
            Date date = new Date();

            long cuurTime = System.currentTimeMillis();
            long timing = aLarmBean01.getDateForLong();

            if(cuurTime > timing){
                String dateStr = DateTimeUtil.getTimeFormatStringFromTimeStamp(timing, "yyyy-MM-dd");
                String dateStr1 = DateTimeUtil.getTimeFormatStringFromTimeStamp(cuurTime, "yyyy-MM-dd");
                O.o(dateStr);
                O.o(date.getTime());
                long s = timing - DateTimeUtil.getTimeStampFromDateFormatString(dateStr);
                long sthis = DateTimeUtil.getTimeStampFromDateFormatString(dateStr1) + s;
                //如果重置后的时间还是小于当前时间，说明今天的定时时间已过，那么就设置为明天的这个时候。这是个坑，当定时的时间在今天过了，一旦设置就会马上执行，显然是不行的，那么就只能设置为明天这个时候了。
                if(sthis < cuurTime){
                    sthis = sthis + DateTimeUtil.ONE_DAY_M;
                }

                date.setTime(sthis);
                O.o(date.getTime());
            }else{
                date.setTime(aLarmBean01.getDateForLong());
            }

            trigger = newTrigger()
                    .withIdentity(name, scheduler.DEFAULT_GROUP)
                    .startAt(date)  // if a start time is not given (if this line were omitted), "now" is implied
                    .withSchedule(simpleSchedule()
                            .withIntervalInHours(24)
                            .withRepeatCount(Integer.MAX_VALUE)) // note that 10 repeats will give a total of 11 firings
                    .forJob(jobDetail) // identify job with handle to its JobDetail itself
                    .build();
        }else{
            if(aLarmBean01.getDateForLong() < System.currentTimeMillis()){
                O.o("单次定时时间已过");
                return;
            }
            Date date = new Date();
            date.setTime(aLarmBean01.getDateForLong());
            trigger = newTrigger()
                    .withIdentity(name, scheduler.DEFAULT_GROUP)
                    .startAt(date)  // if a start time is not given (if this line were omitted), "now" is implied
                    .withSchedule(simpleSchedule()
                            .withIntervalInHours(0)
                            .withRepeatCount(0)) // note that 10 repeats will give a total of 11 firings
                    .forJob(jobDetail) // identify job with handle to its JobDetail itself
                    .build();
        }
        if(trigger != null){
            scheduler.scheduleJob(jobDetail, trigger);
            if(alarmBean01Repository != null){
                alarmBean01Repository.save(aLarmBean01);
            }else{
                O.o("alarmBean01Repository为null");
            }
        }else{
            O.o("定时设置错误trigger为空");
        }

    }
}
