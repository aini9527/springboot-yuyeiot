package com.example.websockettest.timing;

import com.example.websockettest.beans.ControlMessage;
import com.example.websockettest.beans.TimingReCode;
import com.example.websockettest.databaseapi.AlarmBean01Repository;
import com.example.websockettest.databaseapi.TimingRecodeRepository;
import com.example.websockettest.service.MainWebSocket;
import com.example.websockettest.utils.DateTimeUtil;
import com.example.websockettest.utils.MyUtils;
import com.example.websockettest.utils.O;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Service
public class ClockTask implements Job {
    @Autowired
    private AlarmBean01Repository alarmBean01Repository;

    @Autowired
    private TimingRecodeRepository timingRecodeRepository;

    //为了使Autowired标注的字段不为空
    public static ClockTask clockTask;
    @PostConstruct
    public void init() {
        clockTask = this;
    }
    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        String iccid = jobExecutionContext.getJobDetail().getJobDataMap().getString("Iccid");
        int controlBtnIndex = jobExecutionContext.getJobDetail().getJobDataMap().getInt("ControlBtnIndex");
        int switchFlag = jobExecutionContext.getJobDetail().getJobDataMap().getInt("SwitchFlag");
        int alarmType = jobExecutionContext.getJobDetail().getJobDataMap().getInt("AlarmType");
        int alarmFlag = jobExecutionContext.getJobDetail().getJobDataMap().getInt("AlarmFlag");
        long dateForLong = jobExecutionContext.getJobDetail().getJobDataMap().getLong("dateForLong");
        O.o("执行定时  " + iccid + "  /  " + controlBtnIndex + "-" + switchFlag);

        MainWebSocket mainWebSocket = MainWebSocket.webSocketMap.get(iccid);
        if(mainWebSocket != null){
            ControlMessage controlMessage = new ControlMessage();
            controlMessage.setMessageType(0);
            controlMessage.setIccid(iccid);
            controlMessage.setControlBtnIndex(controlBtnIndex);
            controlMessage.setSwitchFlag(switchFlag);
            String s = MyUtils.obj2stringForGson(controlMessage);
            //防止阻塞
            synchronized (mainWebSocket.getSession()){
                try {
                    mainWebSocket.getSession().getBasicRemote().sendText(s);
                    TimingReCode timingReCode = new TimingReCode();
                    timingReCode.setControlBtnIndex(controlBtnIndex);
                    timingReCode.setAlarmFlag(alarmFlag);
                    timingReCode.setControlFlag(switchFlag);
                    timingReCode.setAlarmType(alarmType);
                    timingReCode.setIccid(iccid);
                    timingReCode.setOperationTime(DateTimeUtil.getTimeFormatStringFromTimeStamp(System.currentTimeMillis(), "yyyy-MM-dd HH:mm:ss"));
                    timingReCode.setDateForLong(dateForLong);
                    if(clockTask.timingRecodeRepository == null){
                        O.o("timingRecodeRepository为空！");
                        return;
                    }
                    clockTask.timingRecodeRepository.save(timingReCode);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        //如果是单次定时就删除数据库的数据。
        if(alarmType == 0){
            clockTask.alarmBean01Repository.deleteAllByAlarmFlag(alarmFlag);
        }
    }
}
