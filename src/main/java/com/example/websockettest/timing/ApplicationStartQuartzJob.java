package com.example.websockettest.timing;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.example.websockettest.beans.ALarmBean01;
import com.example.websockettest.databaseapi.AlarmBean01Repository;
import com.example.websockettest.utils.DateTimeUtil;
import com.example.websockettest.utils.O;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.ContextRefreshedEvent;

import javax.xml.crypto.Data;

import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

/**
 * 任务初始化调度
 *
 */
@Configuration
public class ApplicationStartQuartzJob implements ApplicationListener<ContextRefreshedEvent> {
    @Autowired
    private AlarmBean01Repository alarmBean01Repository;
    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {

        List<ALarmBean01> sysTaskList = alarmBean01Repository.findAll();
        Scheduler scheduler=null;
        String className = "com.example.websockettest.timing.ClockTask";
        try {
            scheduler = scheduler();
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
        if(!sysTaskList.isEmpty()){
            for (ALarmBean01 sysTask : sysTaskList) {
                String name = sysTask.getIccid() + "==" + sysTask.getDateForLong() + "==" + sysTask.getAlarmFlag();
                Class classs=null;
                SimpleTrigger trigger;
                try {
                    classs = Class.forName(className);
                } catch (ClassNotFoundException e) {
                    e.printStackTrace();
                }
                JobDetail jobDetail = JobBuilder.newJob(classs).withIdentity(name, scheduler.DEFAULT_GROUP).build();
                jobDetail.getJobDataMap().put("Iccid", sysTask.getIccid());
                jobDetail.getJobDataMap().put("ControlBtnIndex", sysTask.getControlBtnIndex());
                jobDetail.getJobDataMap().put("SwitchFlag", sysTask.getSwitchFlag());
                jobDetail.getJobDataMap().put("AlarmType", sysTask.getAlarmType());
                jobDetail.getJobDataMap().put("AlarmFlag", sysTask.getAlarmFlag());
                jobDetail.getJobDataMap().put("dateForLong", sysTask.getDateForLong());

                if(sysTask.getAlarmType() == 1){
                    Date date = new Date();

                    long cuurTime = System.currentTimeMillis();
                    long timing = sysTask.getDateForLong();
                    //因为是重复定时，如果当前时间大于定时时间 需要重置定时时间
                    if(timing < cuurTime){
                        String dateStr = DateTimeUtil.getTimeFormatStringFromTimeStamp(timing, "yyyy-MM-dd");
                        String dateStr1 = DateTimeUtil.getTimeFormatStringFromTimeStamp(cuurTime, "yyyy-MM-dd");
                        O.o(dateStr);
                        O.o(date.getTime());
                        long s = timing - DateTimeUtil.getTimeStampFromDateFormatString(dateStr);
                        long sthis = DateTimeUtil.getTimeStampFromDateFormatString(dateStr1) + s;
                        //如果重置后的时间还是小于当前时间，说明今天的定时时间已过，那么就设置为明天的这个时候。这是个坑，当定时的时间在今天过了，一旦设置就会马上执行，显然是不行的，那么就只能设置为明天这个时候了。
                        if(sthis < cuurTime){
                            sthis = sthis + DateTimeUtil.ONE_DAY_M;
                        }

                        date.setTime(sthis);
                        O.o(date.getTime());

                    }else{
                        date.setTime(sysTask.getDateForLong());
                    }

                    trigger = newTrigger()
                            .withIdentity(name, scheduler.DEFAULT_GROUP)
                            .startAt(date)  // if a start time is not given (if this line were omitted), "now" is implied
                            .withSchedule(simpleSchedule()
                                    .withIntervalInHours(24)
                                    .withRepeatCount(Integer.MAX_VALUE)) // note that 10 repeats will give a total of 11 firings
                            .forJob(jobDetail) // identify job with handle to its JobDetail itself
                            .build();
                }else{
                    Date date = new Date();
                    date.setTime(sysTask.getDateForLong());
                    if(date.getTime() > System.currentTimeMillis()){
                        trigger = newTrigger()
                                .withIdentity(name, scheduler.DEFAULT_GROUP)
                                .startAt(date)  // if a start time is not given (if this line were omitted), "now" is implied
                                .withSchedule(simpleSchedule()
                                        .withIntervalInHours(0)
                                        .withRepeatCount(0)) // note that 10 repeats will give a total of 11 firings
                                .forJob(jobDetail) // identify job with handle to its JobDetail itself
                                .build();
                    }else{ //单次定时时间已经过去了，删除并不设置定时任务
                        O.o("单次定时时间已过");
                        alarmBean01Repository.deleteAllByAlarmFlag(sysTask.getAlarmFlag());
                        continue;
                    }
                }


                try {
                    scheduler.scheduleJob(jobDetail, trigger);
                } catch (SchedulerException e) {
                    e.printStackTrace();
                }
            }
        }
        try {
            scheduler.start();
        } catch (SchedulerException e) {
            e.printStackTrace();
        }
    }
    /**
     * 初始注入scheduler
     * @return
     * @throws SchedulerException
     */
    @Bean
    public Scheduler scheduler() throws SchedulerException{
        SchedulerFactory schedulerFactoryBean = new StdSchedulerFactory();
        return schedulerFactoryBean.getScheduler();
    }

}